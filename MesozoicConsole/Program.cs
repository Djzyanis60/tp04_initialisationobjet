﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mesozoic;



namespace MesozoicConsole
{
    
        
    public class Program
    {
        public static void Main(string[] args)
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            Dinosaur franklin = new Dinosaur("franklin", "Triceraptos", 18);
            Dinosaur julgo = new Dinosaur("julgo", "Steakausorus", 7);

            List<Dinosaur> dinosaurs = new List<Dinosaur>();

            dinosaurs.Add(louis); //Append dinosaur reference to end of list
            dinosaurs.Add(nessie); //Ajouter la référence au dinosaure à la fin de la liste
            dinosaurs.Add(franklin);
            dinosaurs.Add(julgo);

            //Console.WriteLine(dinosaurs.Count); //Compter le nbr de dinosaure, on peut très bien retirer le count pour enlever le 4.
            //Iterate over our list
            foreach (Dinosaur dino in dinosaurs)  // on l'a apellé dino mais on aurais plus l'apeller comme on le veut. C'est une itératon.
            {
                Console.WriteLine(dino.sayHello());
            }
            Console.WriteLine(louis.hug(louis));
            Console.WriteLine(nessie.hug(louis));
            
            Console.WriteLine("\nCréation d'un troupeau");
            
            Horde horde = new Horde();
            horde.AddDinosaur(louis);
            horde.AddDinosaur(nessie);
            
            Console.WriteLine(horde.IntroduceAll());
            Console.ReadKey();
        }
    }
}



